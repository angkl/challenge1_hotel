const expect = require('chai').expect;
const booking = require("../src/booking")
const config = require("../src/config")

//number of adult+children in a booking must not exceed upper guest limit
describe('Max Guest', function(){
    context(`Adult+Children:6 Limit:${config.MAX_GUEST} Within guest limit(excluding infant)`, function() {
        const guest = new Guest(3,3,3)
        
        it(`Should return without error
        Number of Adult+Children (6) is within guest limit (${config.MAX_GUEST})`, function() {
            const result = booking.distributeGuest(guest)
            expect(result).to.not.have.property('err')
        })
    })
    
    context(`Adult+Children:7 Limit:${config.MAX_GUEST} Within guest limit(excluding infant)`, function() {
        const guest = new Guest(7,0,3)
        
        it(`Should return without error
        Number of Adult+Children (7) is within guest limit (${config.MAX_GUEST})`, function() {
            const result = booking.distributeGuest(guest)
            expect(result).to.not.have.property('err')
        })
    })
    
    context(`Adult+Children:8 Limit:${config.MAX_GUEST} Exceeds guest limit(excluding infant)`, function() {
        const guest = new Guest(2,6,3)
        
        it(`Should return with "Max Guest" error
        Number of Adult+Children (8) exceeds guest limit (${config.MAX_GUEST})`, function() {
            const result = booking.distributeGuest(guest)
            expect(result.err.type).to.equal(booking.ERR_ACGUEST)
        })
    })
    
    context(`Adult+Children:11 Limit:${config.MAX_GUEST} Exceeds guest limit(excluding infant)`, function() {
        const guest = new Guest(3,8,3)
        
        it(`Should return with "Max Guest" error
        Number of Adult+Children (11) exceeds guest limit (${config.MAX_GUEST})`, function() {
            const result = booking.distributeGuest(guest)
            expect(result.err.type).to.equal(booking.ERR_ACGUEST)
        })
    })
    
    context(`Adult+Children:9 Limit:${config.MAX_GUEST} Exceeds guest limit(excluding infant)`, function() {
        const guest = new Guest(7,2,3)
        
        it(`Should return with "Max Guest" error
        Number of Adult+Children (9) exceeds guest limit (${config.MAX_GUEST})`, function() {
            const result = booking.distributeGuest(guest)
            expect(result.err.type).to.equal(booking.ERR_ACGUEST)
        })
    })
})

//number of rooms needed must not exceed upper room limit
describe('Max Room', function(){
    context(`Room:2 Limit:${config.MAX_ROOM} Within limit`, function() {
        const guest = new Guest(4,2,6)
        
        it(`Should return without error
        Number of rooms (2) is within limit (${config.MAX_ROOM})`, function() {
            const result = booking.distributeGuest(guest)
            expect(result).to.not.have.property('err')
        })
    })
    
    context(`Room:4 Limit:${config.MAX_ROOM} Exceeds limit`, function() {
        const guest = new Guest(4,2,11)
        
        it(`Should return with "Max Room" error
        Number of rooms (4) exceeds limit (${config.MAX_ROOM})`, function() {
            const result = booking.distributeGuest(guest)
            expect(result.err.type).to.equal(booking.ERR_MAX_ROOM)
        })
    })
})

//number of adult must be greater than or equal to number of rooms to meet supervision criteria
describe('Adult Supervision', function(){
    context(`Adult:1 Room:1 Adult==Room Supervision Criteria Met`, function() {
        const guest = new Guest(1,3,3)
        
        it(`Should return without error
        Number of adults (1) is equal to the number of rooms needed (1). 
        Adult supervision criteria is met`, function() {
            const result = booking.distributeGuest(guest)
            expect(result).to.not.have.property('err')
        })
    })
    
    context(`Adult:2 Room:2 Adult==Room Supervision Criteria Met`, function() {
        const guest = new Guest(2,4,3)
        
        it(`Should return without error
        Number of adults (2) is equal to the number of rooms needed (2). 
        Adult supervision criteria is met`, function() {
            const result = booking.distributeGuest(guest)
            expect(result).to.not.have.property('err')
        })
    })
    
    context(`Adult:3 Room:2 Adult>Room Supervision Criteria Met`, function() {
        const guest = new Guest(3,2,6)
        
        it(`Should return without error
        Number of adults (3) is greater than the number of rooms needed (2). 
        Adult supervision criteria is met`, function() {
            const result = booking.distributeGuest(guest)
            expect(result).to.not.have.property('err')
        })
    })
    
    context(`Adult:7 Room:3 Adult>Room Supervision Criteria Met`, function() {
        const guest = new Guest(7,0,3)
        
        it(`Should return without error
        Number of adults (7) is greater than the number of rooms needed (3). 
        Adult supervision criteria is met`, function() {
            const result = booking.distributeGuest(guest)
            expect(result).to.not.have.property('err')
        })
    })
    
    context(`Adult:0 Room:1 Adult<Room Supervision Error`, function() {
        const guest = new Guest(0,3,0)
        
        it(`Should return "Supervision" error
        Number of adults (0) is fewer than the number of rooms needed (1). 
        Adult supervision criteria is violated`, function() {
            const result = booking.distributeGuest(guest)
            expect(result.err.type).to.equal(booking.ERR_SUPERV)
        })
    })
    
    context(`Adult:1 Room:2 Adult<Room Supervision Error`, function() {
        const guest = new Guest(1,4,0)
        
        it(`Should return "Supervision" error
        Number of adults (1) is fewer than the number of rooms needed (2). 
        Adult supervision criteria violated`, function() {
            const result = booking.distributeGuest(guest)
            expect(result.err.type).to.equal(booking.ERR_SUPERV)
        })
    })
    
    context(`Adult:2 Room:3 Adult<Room Supervision Error`, function() {
        const guest = new Guest(2,2,7)
        
        it(`Should return "Supervision" error
        Number of adults (2) is fewer than the number of rooms needed (3). 
        Adult supervision criteria is violated`, function() {
            const result = booking.distributeGuest(guest)
            expect(result.err.type).to.equal(booking.ERR_SUPERV)
        })
    })
})

describe('Distribution', function(){
    context(`Adult:3 Children:3 Infant:3`, function() {
        const guest = new Guest(3,3,3)
        const expGuest0 = new Guest(3,3,3)
        
        it(`Should have distribution of 
        {no:0, adult:${expGuest0.adult}, children:${expGuest0.children}, infant:${expGuest0.infant}}. 
        1 room in total`, function() {
            const result = booking.distributeGuest(guest)
            expectRoomCount(result,1)
            expectDistribution(result,0,expGuest0)
        })
    })
    
  
    context(`Adult:3 Children:4 Infant:2`, function() {
        const guest = new Guest(3,4,2)
        const expGuest0 = new Guest(2,2,1)
        const expGuest1 = new Guest(1,2,1)
        
        it(`Should have distribution of 
        {no:0, adult:${expGuest0.adult}, children:${expGuest0.children}, infant:${expGuest0.infant}}, 
        {no:1, adult:${expGuest1.adult}, children:${expGuest1.children}, infant:${expGuest1.infant}}. 
        2 rooms in total`, function() {
            const result = booking.distributeGuest(guest)
            expectRoomCount(result,2)
            expectDistribution(result,0,expGuest0)
            expectDistribution(result,1,expGuest1)
        })
    })
    
    context(`Adult:2 Children:3 Infant:4`, function() {
        const guest = new Guest(2,3,4)
        const expGuest0 = new Guest(1,2,2)
        const expGuest1 = new Guest(1,1,2)
        
        it(`Should have distribution of 
        {no:0, adult:${expGuest0.adult}, children:${expGuest0.children}, infant:${expGuest0.infant}}, 
        {no:1, adult:${expGuest1.adult}, children:${expGuest1.children}, infant:${expGuest1.infant}}. 
        2 rooms in total`, function() {
            const result = booking.distributeGuest(guest)
            expectRoomCount(result,2)
            expectDistribution(result,0,expGuest0)
            expectDistribution(result,1,expGuest1)
        })
    })
    
    context(`Adult:7 Children:0 Infant:5`, function() {
        const guest = new Guest(7,0,5)
        const expGuest0 = new Guest(3,0,2)
        const expGuest1 = new Guest(2,0,2)
        const expGuest2 = new Guest(2,0,1)
        
        it(`Should have distribution of 
        {no:0, adult:${expGuest0.adult}, children:${expGuest0.children}, infant:${expGuest0.infant}}, 
        {no:1, adult:${expGuest1.adult}, children:${expGuest1.children}, infant:${expGuest1.infant}}, 
        {no:2, adult:${expGuest2.adult}, children:${expGuest2.children}, infant:${expGuest2.infant}}. 
        3 rooms in total`, function() {
            const result = booking.distributeGuest(guest)
            expectRoomCount(result,3)
            expectDistribution(result,0,expGuest0)
            expectDistribution(result,1,expGuest1)
            expectDistribution(result,2,expGuest2)
        })
    })
    
    context(`Adult:3 Children:3 Infant:7`, function() {
        const guest = new Guest(3,3,7)
        const expGuest0 = new Guest(1,1,3)
        const expGuest1 = new Guest(1,1,2)
        const expGuest2 = new Guest(1,1,2)
        
        it(`Should have distribution of 
        {no:0, adult:${expGuest0.adult}, children:${expGuest0.children}, infant:${expGuest0.infant}}, 
        {no:1, adult:${expGuest1.adult}, children:${expGuest1.children}, infant:${expGuest1.infant}}, 
        {no:2, adult:${expGuest2.adult}, children:${expGuest2.children}, infant:${expGuest2.infant}}. 
        3 rooms in total`, function() {
            const result = booking.distributeGuest(guest)
            expectRoomCount(result,3)
            expectDistribution(result,0,expGuest0)
            expectDistribution(result,1,expGuest1)
            expectDistribution(result,2,expGuest2)
        })
    })
})

function Guest(adult, children, infant){
    this.adult = adult
    this.children = children
    this.infant = infant
}

function expectRoomCount(distribution, expRoomCount){
    expect(distribution.rooms.length).to.equal(expRoomCount)
}
function expectDistribution(distribution,roomNo, expGuest){
    const room = distribution.rooms[roomNo]
    expect(room.adult).to.equal(expGuest.adult)
    expect(room.children).to.equal(expGuest.children)
    expect(room.infant).to.equal(expGuest.infant)
}
