## Challenge

**Background:**  
While booking a hotel, there are many restrictions or limitations like maximum number of guests
or maximum adults allowed in one room etc. It�s programmer�s job to translate these restrictions
to website/app visitors while booking a hotel room online.

**Problem:**  
In Hotel Transylvania, the following rules apply    
1. each room can only accommodate a maximum of 3 adults  AND  3 children  AND 3 infants  
2. In one booking, maximum guests can be 7(excluding infants)  
3. No room will have only children or infants (i.e without at least one adult supervision)  
4. Per booking maximum number of rooms will be only 3  
5. If guests are either greater than 7(excluding infants) or rooms are greater than 3, the
booking will be rejected.  

**Task:**  
Write a program to input number of adults, children and infants to output the mapping of pax per
room. Optimise number of rooms to keep it to minimum.  
For example:  
For 3 adults, 4 children and 2 infants can be fit in 2 rooms and not 3.


## Assumptions
Below are the assumptions made
1. Maximum guests can be 7(excluding infants), thus a booking of 3 adults, 4 children, and 9 infants is considered valid.

## Getting Started

### Prerequisites

Go to http://nodejs.org/, download node.js installer and follow instructions. Mac OS, Windows, and Linux are supported.

Run the following to install dependencies.
```bash
npm install
```

Run the following to install mocha for testing.
```bash
npm install -g mocha
```



## Tests
### Local
Run the following to start listening to listen to assigned port.
```bash
npm start
```  
  
The following takes adult (3), children (3), and infant (6) as parameters. These parameters are defaulted to 0.

[http://localhost:8001/roomdistribution?adult=3&children=3&infant=6](http://localhost:8001/roomdistribution?adult=2&children=3&infant=6)

### Hosted
The code is hosted on App Engine  
The following takes adult (3), children (4), and infant (2) as parameters. These parameters are defaulted to 0.

[https://booking-dot-akl-challenge.appspot.com/roomdistribution?adult=3&children=4&infant=2](https://booking-dot-akl-challenge.appspot.com/roomdistribution?adult=3&children=4&infant=2)

*Note that it might take a few seconds (due to cold start) to get the result on your first invocation.
### Mocha Test
Run the following to test the room-distribution function
```bash
npm test
```
Here are the [test results](https://bitbucket.org/angkl/challenge1_hotel/src/master/testresults.txt).