const koa = require('koa')
const cors = require('koa2-cors');
const koa_router = require('koa-router')
const booking = require('./src/booking')
const app = new koa()
const router = new koa_router()
const mw = require('./src/mw')

router.get('/roomDistribution', ctx=> {
    ctx.body = booking.distributeGuest(ctx.state.guest)
})

const PORT = process.env.PORT || 8001;
app
    .use(cors())
    .use(mw.errHand)
    .use(mw.checkPara)
    .use(router.routes())
    .use(router.allowedMethods())
    .listen(PORT, () => {
      console.log(`App listening on port ${PORT}`);
      console.log('Press Ctrl+C to quit.');
});
