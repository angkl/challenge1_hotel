const config = require('./config')
const ERR_SUPERV = "0"
const ERR_MAX_ROOM = "1"
const ERR_ACGUEST = "2"

module.exports = {
    distributeGuest : distributeGuest,
    ERR_SUPERV :ERR_SUPERV,
    ERR_MAX_ROOM : ERR_MAX_ROOM,
    ERR_ACGUEST : ERR_ACGUEST
}

function distributeGuest(guest){
    const result ={}
    const rooms = []
    
    const acGuestCount = guest.adult + guest.children
  
    //Adult + Children must be less than Guest Limt
    if(acGuestCount > config.MAX_GUEST){
        result.err = {
            type : ERR_ACGUEST,
            errMsg : `Maximum guest count(excluding infants) is ${config.MAX_GUEST}. Submitted guest count(excluding infants) is ${acGuestCount}`
        }
    }//if(acGuestCount > 7){
    else{
        const roomsNeeded = Math.max(
        calRoomsNeeded(guest.adult,config.MAX_ADULTS_PER_ROOM), 
        calRoomsNeeded(guest.children,config.MAX_CHILDREN_PER_ROOM), 
        calRoomsNeeded(guest.infant,config.MAX_INFANT_PER_ROOM) )
            
        //if number of rooms needed exceed Room Limit
        if(roomsNeeded > config.MAX_ROOM){
            result.err = {
                type : ERR_MAX_ROOM,
                errMsg : `Number of rooms (${roomsNeeded}) exceeded maximum number (${config.MAX_ROOM})`
            }
        }//if(roomsNeeded > config.MAX_ROOM){}
        //A room needs at least an adult supervision
        else if( roomsNeeded > guest.adult){
            result.err = {
                type : ERR_SUPERV,
                errMsg : `A room must have at least one adult supervision. Number of adults (${guest.adult}), number of rooms (${roomsNeeded})`
            }
        }//else if( roomsNeeded > guest.adult){
        else{
            let adultsLeft = guest.adult
            let childrenLeft = guest.children
            let infantsLeft = guest.infant
            for(roomNo=0; roomNo<roomsNeeded; roomNo++){
                const roomsLeft = roomsNeeded-roomNo
                const adultPerRoom = distributeRoom(adultsLeft, roomsLeft)
                const childrenPerRoom = distributeRoom(childrenLeft,roomsLeft)
                const infantPerRoom = distributeRoom(infantsLeft, roomsLeft)
                
                adultsLeft -= adultPerRoom
                childrenLeft -= childrenPerRoom
                infantsLeft -= infantPerRoom
                
                const room = {
                    no : roomNo,
                    adult : adultPerRoom,
                    children : childrenPerRoom,
                    infant : infantPerRoom,
                }//room
                rooms.push(room)
                result.rooms = rooms
            }//for(int i=0; i<roomsNeeded; i++){
        }//else from if(roomsNeeded > config.MAX_ROOM){
    }//else from if(acGuestCount > 7){
    
    return result
}


function calRoomsNeeded(totalPax, maxPax){
    return Math.ceil(totalPax/maxPax)
}

//spreaded distribution
function distributeRoom(paxLeft, roomsLeft){
    return parseInt( Math.ceil(paxLeft/roomsLeft) )
}

//front-loaded distribution
function distributeFlRoom(paxLeft, maxPax){
    let count 
    if(paxLeft >= maxPax){
        count = maxPax
    }
    else{
        count = paxLeft
    }
    return count
}


