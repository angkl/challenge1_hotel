module.exports = {
    errHand : errHand,
    checkPara : checkPara
}

async function errHand(ctx, next){
    try{
        await next()
    }catch (err){
        const msg = `${err.message}`
        ctx.body = msg
        console.log(msg)
    }
}

async function checkPara(ctx,next){
    const reqPara = ctx.request.query
    
    ctx.state.guest = {
        adult : parseInt(reqPara.adult) || 0 ,
        children : parseInt(reqPara.children) || 0,
        infant : parseInt(reqPara.infant) || 0
    }
    await next()
}
